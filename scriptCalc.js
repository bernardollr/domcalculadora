let numeros = [];
let contador = 0;

function enviarNumero(event) {
    event.preventDefault();
    let input = document.getElementById("input").value;
    let numero = parseFloat(input);
    numeros.push(numero);
    contador++;
    let output = document.getElementById("output");
    output.innerHTML = "Numeros: " + numeros.join(", ") + "<br>Contador: " + contador;
    document.getElementById("input").value = "";
}

function calcularMedia() {
  let soma = 0;
  for (let i = 0; i < numeros.length; i++) {
    soma += numeros[i];
  }
  let media = soma / numeros.length;
  media = parseFloat(media).toFixed(2)
  let mediaBox = document.getElementById("media");
  mediaBox.innerHTML = "Média: " + media;
}
